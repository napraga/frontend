export const apiConfig = {
  baseURL: (process.env.NODE_ENV === 'development') ? 'http://localhost:8000' : 'http://api.ynsitu.com',
  login: '/api/login',
  register: '/api/register',
  products: '/api/products',
  product_id: '/api/product_id',
  products_category: '/api/products_category',
  categories: '/api/categories',
  mycompanies: '/api/mycompanies',
  mystores: '/api/mystores',
  myproduct: '/api/myproduct',
  stores: '/api/stores',
  products_search: '/api/products_search'
}
