import axios from 'axios'
import Vue from 'vue'

import { apiConfig } from '~/constants'

const headers = (token) => {
  return {
    validateStatus: false,
    headers: {
      'Content-Type': 'application/json',
      Authorization: token ? `Bearer ${token.token}` : ''
    }
  }
}

export const methodGet = async (endpoint, token = null, callback = () => {}, dataBody = '') => {
  await axios.get(
    `${apiConfig.baseURL}${apiConfig[endpoint] + dataBody}`,
    headers(token)
  ).then((response) => {
    callback(response)
  })
}

export const methodPost = async (endpoint, dataBody = {}, token = null, callback = () => {}) => {
  await axios.post(`${apiConfig.baseURL}${apiConfig[endpoint]}`, dataBody, headers(token)).then((response) => {
    callback(response)
  })
}

export const methodPut = async (endpoint, dataBody = {}, token = null, callback = () => {}) => {
  return await axios.put(`${apiConfig.baseURL}${apiConfig[endpoint]}`, dataBody, headers(token)).then((response) => {
    callback(response)
  })
}

Vue.prototype.$methodGet = methodGet
Vue.prototype.$methodPost = methodPost
Vue.prototype.$methodPut = methodPut
