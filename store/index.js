export const state = () => {
}

export const mutations = {
  addsignedIn (state, data) {
    state.signedIn = data
  },
  removesignedIn (state) {
    state.signedIn = false
  },
  addCartItem (state, data) {
    if (typeof state.cart === 'undefined') {
      state.cart = []
    }
    state.cart.push(data)
  },
  removeCartItem (state, payload) {
    // const index = state.cart.indexOf(payload)
    state.cart.splice(payload, 1)
  }
}

export const getters = {
  isAuthenticated (state) {
    return (state.signedIn) ? state.signedIn : false
  },
  showCart (state) {
    return (state.cart) ? state.cart : []
  }
}
